*****************************************************
********** Para correr el servidor ******************
*****************************************************

source tfm-venv/bin/activate
python manage.py runserver

*****************************************************

Si se elimina el schema de la base de datos "tfm" hacer lo siguiente
psql -d tfm -U tfmuser -W
- dentro de postgre
create schema public;

Luego hay que crear el nuevo superuser (salir de psql en la terminal y verificar que sigue en el virtual env)
python manage.py createsuperuser

Cada vez que haya un cambio en la base de datos aplicar en consola en el directorio del proyecto a la altura del fichero manage.py
python manage.py makemigrations
python manage.py migrate
