from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^company_information', company_information, name='company_information'),
    url(r'^contact_us', contact_us, name='contact_us'),
    url(r'^success', successView, name='success'),
]
