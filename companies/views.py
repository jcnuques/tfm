# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail, BadHeaderError
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import *


# Create your views here.

@login_required
def company_information(request):
    pk = request.user.company.id
    company = Company.objects.get(id=pk)
    form = CompanyInformationForm(instance=company)

    if request.method == 'POST':
        form = CompanyInformationForm(request.POST)
        if form.is_valid():
            company.name = form.cleaned_data['name']
            company.address = form.cleaned_data['address']
            company.phone = form.cleaned_data['phone']
            company.email = form.cleaned_data['email']
            company.save()
            return redirect('/')
        else:
            return render(request, 'companies/company_information.html', {'form': form})

    return render(request, 'companies/company_information.html', {'form': form, 'pk': pk})


def contact_us(request):
    form = ContactUsForm()

    if request.method == 'POST':
        form = ContactUsForm(request.POST)
        if form.is_valid():
            subject = "New company interest"
            from_email = form.cleaned_data['contact_email']
            message = 'Company name: ' + form.cleaned_data['company_name'] + '\n' + \
                      'Contact: ' + form.cleaned_data['contact_name'] + '\n' + \
                      'Email: ' + form.cleaned_data['contact_email'] + '\n' + \
                      'Phone: ' + form.cleaned_data['contact_phone'] + '\n' + \
                      'Message: ' + form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['sobman.app@gmail.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect('success')
        else:
            return render(request, 'contact_us.html', {'form': form})

    return render(request, 'contact_us.html', {'form': form})


def successView(request):
    # return HttpResponse('Success! Thank you for your message.')
    return render(request, 'success.html')
