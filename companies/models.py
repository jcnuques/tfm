# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import RegexValidator
from django.urls import reverse

# Create your models here.


class Company(models.Model):
    name = models.fields.CharField(max_length=200, null=False, blank=False)
    address = models.fields.CharField(max_length=150)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. 15 digits allowed.")
    phone = models.CharField(validators=[phone_regex], max_length=17, blank=True)  # validators should be a list
    email = models.EmailField(blank=True)
    created_at = models.DateField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'company'
        verbose_name_plural = 'companies'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        """
        Retorna la url para acceder a una instancia particular de una venta.
        """
        return reverse('company_information', args=[str(self.id)])
