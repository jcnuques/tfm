from django import forms
from .models import *

class CompanyInformationForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = '__all__'


class ContactUsForm(forms.Form):
    company_name = forms.CharField(max_length=100, label="Company name")
    contact_name = forms.CharField(max_length=100, label="Contact name")
    contact_email = forms.EmailField(max_length=100, label="Email")
    contact_phone = forms.CharField(max_length=45, label="Phone")
    message = forms.CharField(widget=forms.Textarea, required=False)
