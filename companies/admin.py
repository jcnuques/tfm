# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from companies.models import Company

# Register your models here.


class TheCompanyAdmin(admin.ModelAdmin):
    model = Company
    exclude = ("created_at",)
    list_display = ('name', 'address', 'phone', 'created_at', 'is_deleted')

    def get_queryset(self, request):
        qs = super(TheCompanyAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(id=request.user.company_id)


admin.site.register(Company, TheCompanyAdmin)
