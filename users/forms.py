from django.forms.models import ModelForm
from django import forms
from .models import *
from dal import autocomplete
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordResetForm


class CustomerCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CustomerCreateForm, self).__init__(*args, **kwargs)
        self.fields['seller'].queryset = Seller.objects.filter(company_id=kwargs['initial']['company_id'],
                                                               is_active=True)


    class Meta:
        model = Customer
        fields = ['name', 'identification', 'address', 'phone', 'email', 'seller']
        widgets = {
            'seller': autocomplete.ModelSelect2(),
        }


class SellerCreateForm(UserCreationForm):
    class Meta:
        model = Seller
        fields = ['first_name', 'last_name', 'email', 'phone', 'password1', 'password2']

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class SupervisorCreateForm(UserCreationForm):
    class Meta:
        model = Supervisor
        fields = ['first_name', 'last_name', 'email', 'phone', 'password1', 'password2', 'is_active']

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class SellerEditForm(forms.ModelForm):
    class Meta:
        model = Seller
        exclude = ['password']
        fields = ['first_name', 'last_name', 'email', 'phone', 'is_active']


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'phone']


class CustomerFilterForm(forms.Form):
    search_field = forms.CharField(max_length=100, required=False, label="Search by ID/Name")
