# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from users.models import *
from dal import autocomplete


class CustomerAdmin(admin.ModelAdmin):
    model = Customer
    exclude = ('created_at',)
    fieldsets = (
        ('Customer info', {'fields': ('company', 'seller', 'name', 'address', 'phone', 'email', 'is_deleted')}),
    )
    list_display = ('company', 'seller', 'name', 'address', 'phone', 'email', 'created_at', 'is_deleted')


class SupervisorAdmin(UserAdmin):
    model = Supervisor
    exclude = ("last_login", "user_permissions", "date_joined", 'is_superuser')
    fieldsets = (
        ('Personal info', {'fields': ('first_name', 'last_name', 'company', 'username', 'email', 'password', 'phone',
                                      'is_active', 'is_staff')}),
        ('Permissions', {'fields': ('groups',)}),
    )
    list_display = ('first_name', 'last_name', 'company', 'email', 'phone', 'date_joined', 'is_active')

    widgets = {
        'company': autocomplete.ModelSelect2(url='company-autocomplete'),
    }

    # def has_change_permission(self, request, user_to_edit=None):
    #
    #     is_supervisor = user_is_in_group(request.user, 'Supervisors')
    #     if user_to_edit is not None:
    #         user_to_edit_is_salesperson = user_is_in_group(user_to_edit, 'Sellers')
    #
    #         return (is_supervisor and user_to_edit_is_salesperson) or request.user.is_superuser or user_to_edit.id == request.user.id
    #     else:
    #         return request.user.is_superuser or is_supervisor
    #
    # def has_add_permission(self, request):
    #     is_supervisor = user_is_in_group(request.user, 'Supervisors')
    #
    #     return (is_supervisor and request.user.is_superuser) or request.user.is_superuser

    # def has_delete_permission(self, request, user_to_delete=None):
    #     is_supervisor = user_is_in_group(request.user, 'Supervisors')
    #
    #     if is_supervisor:
    #         # voy a borrar varios a la vez entonces necesito ver que sean sellers
    #         if request.POST and request.POST.get('action') == 'delete_selected':
    #             seller_ids = list(request.POST.getlist('_selected_action'))
    #             real_sellers = Seller.objects.all().values_list('id', flat=True)
    #             real_sellers = [str(x) for x in real_sellers]
    #
    #             return seller_ids in real_sellers
    #     else:
    #         return request.user.is_superuser

    def get_queryset(self, request):
        qs = super(SupervisorAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.company_id)


class SellerAdmin(UserAdmin):
    model = Seller
    exclude = ("last_login", "user_permissions", "date_joined", 'is_staff', 'is_superuser')
    fieldsets = (
        ('Personal info', {'fields': ('company', 'first_name', 'last_name', 'username', 'email', 'password', 'phone', 'is_active')}),
        ('Permissions', {'fields': ('groups',)})
    )
    list_display = ('first_name', 'last_name', 'company', 'email', 'phone', 'date_joined', 'is_active')

    def get_queryset(self, request):
        qs = super(SellerAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.company_id)


class UserAdmin(UserAdmin):
    model = User
    list_display = ('username', 'first_name', 'last_name', 'email', 'is_staff', 'is_superuser', 'date_joined')

    def get_queryset(self, request):
        qs = super(UserAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.company_id)


class TheCompanyAdminUser(UserAdmin):
    model = CompanyAdmin
    exclude = ("last_login", "user_permissions", "date_joined", 'is_superuser')
    fieldsets = (
        ('Personal info', {'fields': ('company', 'first_name', 'last_name', 'username', 'email', 'password', 'phone', 'is_staff',
                                      'is_active')}),
        ('Permissions', {'fields': ('groups',)})
    )
    list_display = ('username', 'first_name', 'last_name', 'email', 'is_staff', 'is_superuser', 'date_joined')

    def get_queryset(self, request):
        qs = super(TheCompanyAdminUser, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.company_id)


admin.site.register(User, UserAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Seller, SellerAdmin)
admin.site.register(Supervisor, SupervisorAdmin)
admin.site.register(CompanyAdmin, TheCompanyAdminUser)


"""
This function checks if a user belongs to an specific group
"""


def user_is_in_group(user, group_name=""):
    # SELECT * from user.groups where name = group_name AND user = oriana
    return user.groups.filter(name=group_name).exists()

