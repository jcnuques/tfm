# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Q
from django.core.validators import RegexValidator
from companies.models import Company
from django.urls import reverse


class User(AbstractUser):
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = models.fields.CharField(validators=[phone_regex], max_length=45, null=False, blank=False)

    company = models.ForeignKey(Company, null=True)

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def is_seller(self):
        try:
            seller = (self.seller is not None)
        except Seller.DoesNotExist:
            seller = False
        return seller

    def is_supervisor(self):
        try:
            supervisor = (self.supervisor is not None)
        except Supervisor.DoesNotExist:
            supervisor = False
        return supervisor

    def is_companyadmin(self):
        try:
            company_admin = (self.companyadmin is not None)
        except CompanyAdmin.DoesNotExist:
            company_admin = False
        return company_admin


class CompanyAdmin(User):
    class Meta:
        verbose_name = 'Company Admin'
        verbose_name_plural = 'Company Admins'

    def save(self, *args, **kwargs):
        self.email = self.username
        super(CompanyAdmin, self).save(*args, **kwargs)


class Supervisor(User):
    class Meta:
        verbose_name = 'supervisor'
        verbose_name_plural = 'supervisors'

    def save(self, *args, **kwargs):
        self.is_staff = False
        self.username = self.email
        super(Supervisor, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('supervisor-detail', args=[str(self.id)])


class Seller(User):
    class Meta:
        verbose_name = 'seller'
        verbose_name_plural = 'sellers'

    def save(self, *args, **kwargs):
        self.is_staff = False
        self.username = self.email
        super(Seller, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('seller-detail', args=[str(self.id)])


class Customer(models.Model):
    company = models.ForeignKey(Company)
    seller = models.ForeignKey(Seller)
    identification = models.CharField(max_length=20)
    name = models.CharField(max_length=200, null=False, blank=False)
    address = models.TextField(max_length=100, null=False, blank=False)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. \n"
                                         " Up to 15 digits allowed.")
    phone = models.CharField(validators=[phone_regex], max_length=17, blank=True)  # validators should be a list
    email = models.EmailField(max_length=100, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'customer'
        verbose_name_plural = 'customers'

    # This method is called right before a register is being saved,
    # since customer can't login into the admin panel we set the is_staff field to False ALWAYS.

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.is_staff = False
        # self.name = self.email
        super(Customer, self).save(*args, **kwargs)

    def get_absolute_url(self):
        """
        Retorna la url para acceder a una instancia particular de un cliente.
        """
        return reverse('customer-detail', args=[str(self.id)])


def get_seller_customers(seller_id, search):
    if search is None:
        customers_list = Customer.objects.filter(seller_id=seller_id,
                                                 is_deleted=False).order_by('-created_at')
    else:
        customers_list = Customer.objects.filter(Q(seller_id=seller_id) &
                                                 Q(is_deleted=False) &
                                                 (Q(name__icontains=search) |
                                                  (Q(identification__icontains=search)))).order_by('-created_at')
    return customers_list


def get_company_customers(company_id, search):
    if search is None:
        customers_list = Customer.objects.filter(company_id=company_id,
                                                 is_deleted=False).order_by('-name')
    else:
        customers_list = Customer.objects.filter(Q(company_id=company_id) &
                                                 Q(is_deleted=False) &
                                                 (Q(name__icontains=search) |
                                                  (Q(identification__icontains=search)))).order_by('-name')
    return customers_list
