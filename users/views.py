# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin

from sales.models import Sale, get_seller_sales_count, get_seller_sales_amount, get_seller_sales_list, \
    get_seller_sales_objective
from .forms import *


@login_required()
def index(request):
    """
    Users home page.
    """

    if request.user.is_authenticated:

        if request.user.is_seller():
            current_month_sales_count = get_seller_sales_count(request.user.id, None, None, None)
            current_month_sales_amount = get_seller_sales_amount(request.user.id, None, None, None)
            current_month_sales_objective = get_seller_sales_objective(request.user.id, None, None)
            sales_list = get_seller_sales_list(request.user.id, None, None, None)
            return render(
                request,
                'index.html',
                context={'current_month_sales_count': current_month_sales_count,
                         'current_month_sales_amount': current_month_sales_amount,
                         'current_month_sales_objective': current_month_sales_objective,
                         'sales_list': sales_list}
            )
        elif request.user.is_supervisor():
            sellers_list = Seller.objects.filter(company=request.user.company,
                                                 is_active=True).order_by('first_name')
            for seller in sellers_list:
                seller.current_month_sales_amount = get_seller_sales_amount(seller.id, None, None, None)
                seller.current_month_sales_objective = get_seller_sales_objective(seller.id, None, None)

            sales_list = Sale.objects.filter(seller__company=request.user.company,
                                             sale_date__year=datetime.date.today().year,
                                             sale_date__month=datetime.date.today().month,
                                             is_deleted__exact=False).order_by('-sale_date')
            return render(
                request,
                'index.html',
                context={'sellers_list': sellers_list,
                         'sales_list': sales_list}
            )
        elif request.user.is_companyadmin():
            return render(
                request,
                'index.html',
            )
        else:
            return render(
                request,
                'index.html',
                context={'unauthorized': True}
            )
    else:
        return redirect('login')


@login_required
def sellers_overview(request):
    sellers_list = Seller.objects.filter(company_id=request.user.company.id).order_by('last_name')

    return render(
        request,
        'users/sellers.html',
        context={'sellers_list': sellers_list}
    )


@login_required
def supervisors_overview(request):
    supervisors_list = Supervisor.objects.filter(company_id=request.user.company.id).order_by('last_name')

    return render(
        request,
        'users/supervisors.html',
        context={'supervisors_list': supervisors_list}
    )


@login_required
def create_customer(request):
    company = Company.objects.filter(name__exact=request.user.company).get()

    if request.user.is_seller():
        seller = Seller.objects.filter(id=request.user.id).get()
        initial_data = {'company_id': Company.objects.filter(id__exact=request.user.company.id).get().id,
                        'seller': seller}
    else:
        initial_data = {'company_id': Company.objects.filter(id__exact=request.user.company.id).get().id}

    if request.method == 'POST':
        form = CustomerCreateForm(request.POST, initial=initial_data)
        if form.is_valid():
            identification = form.cleaned_data['identification']
            customer_exist = Customer.objects.filter(identification=identification,
                                                     company=request.user.company.id,
                                                     is_deleted=False).exists()
            if not customer_exist:
                customer = Customer()
                customer.company = company
                if request.user.is_seller():
                    customer.seller = seller
                elif request.user.is_supervisor():
                    customer.seller = form.cleaned_data['seller']
                customer.name = form.cleaned_data['name']
                customer.identification = identification
                customer.address = form.cleaned_data['address']
                customer.phone = form.cleaned_data['phone']
                customer.email = form.cleaned_data['email']
                customer.save()
                return redirect('seller_customers')
            else:
                error_message = "Customer already exists"
                return render(request,
                              'customers/create_customer.html',
                              {'form': form,
                               'error_message': error_message}
                              )
        else:
            return render(request, 'customers/create_customer.html', {'form': form})

    else:
        form = CustomerCreateForm(initial=initial_data)
        return render(
            request,
            'customers/create_customer.html',
            {'form': form})


@login_required
def sellers_customers(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = CustomerFilterForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            search = form.cleaned_data['search_field']
            seller_customers_list = get_company_customers(request.user.company.id, search)
            return render(
                request,
                'customers/seller_customers.html',
                context={'seller_customers_list': seller_customers_list,
                         'form': form},
            )
    else:
        if request.user.is_seller():
            form = CustomerFilterForm()
            seller_customers_list = get_company_customers(request.user.company.id, None)
            return render(
                request,
                'customers/seller_customers.html',
                context={'seller_customers_list': seller_customers_list,
                         'form': form}
            )
        elif request.user.is_supervisor():
            form = CustomerFilterForm()
            seller_customers_list = get_company_customers(request.user.company.id, None )
            return render(
                request,
                'customers/seller_customers.html',
                context={'seller_customers_list': seller_customers_list,
                         'form': form}
            )


@login_required
def customer_detail_view(request, pk):
    initial_data = {'company_id': Company.objects.filter(id__exact=request.user.company.id).get().id}
    customer = Customer.objects.get(id=pk)
    if request.method == 'POST':
        form = CustomerCreateForm(request.POST, initial=initial_data)
        if form.is_valid():
            customer.name = form.cleaned_data["name"]
            customer.identification = form.cleaned_data["identification"]
            customer.address = form.cleaned_data["address"]
            customer.phone = form.cleaned_data["phone"]
            customer.email = form.cleaned_data["email"]
            customer.save()
            return redirect('seller_customers')
        else:
            return render(request, 'customers/customer_detail.html', {'form': form, 'pk': pk})
    else:
        form = CustomerCreateForm(instance=customer, initial=initial_data)
        return render(request, 'customers/customer_detail.html', {'form': form, 'pk': pk})


@login_required
def customer_delete(request, pk):
    customer = Customer.objects.get(id=pk)
    customer.is_deleted = True
    customer.save()
    return redirect('seller_customers')


@login_required
def sellers_create(request):
    if request.method == 'POST':
        form = SellerCreateForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['email']
            user_exist = Supervisor.objects.filter(username=username, is_active=True).exists()
            if not user_exist:
                seller = Seller()
                seller.first_name = form.cleaned_data['first_name']
                seller.last_name = form.cleaned_data['last_name']
                seller.email = form.cleaned_data['email']
                seller.phone = form.cleaned_data['phone']
                seller.set_password(form.cleaned_data['password1'])
                seller.company = Company.objects.filter(id=request.user.company.id).get()
                seller.is_active = True
                seller.save()
                return redirect('sellers_overview')
            else:
                error_message = "User already exists."
                return render(request,
                              'users/sellers_create.html',
                              {'form': form,
                               'error_message': error_message}
                              )
        else:
            return render(request, 'users/sellers_create.html', {'form': form})
    else:
        form = SellerCreateForm()
        return render(
            request,
            'users/sellers_create.html',
            {'form': form})


@login_required
def supervisors_create(request):
    if request.method == 'POST':
        form = SupervisorCreateForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['email']
            user_exist = Supervisor.objects.filter(username=username, is_active=True).exists()
            if not user_exist:
                supervisor = Supervisor()
                supervisor.first_name = form.cleaned_data['first_name']
                supervisor.last_name = form.cleaned_data['last_name']
                supervisor.email = form.cleaned_data['email']
                print supervisor.email
                supervisor.phone = form.cleaned_data['phone']
                supervisor.set_password(form.cleaned_data['password1'])
                supervisor.company = Company.objects.filter(id=request.user.company.id).get()
                supervisor.save()
                return redirect('supervisors_overview')
            else:
                error_message = "User already exists."
                return render(request,
                              'users/supervisors_create.html',
                              {'form': form,
                               'error_message': error_message}
                              )
        else:
            return render(request, 'users/supervisors_create.html', {'form': form})
    else:
        form = SupervisorCreateForm()
        return render(
            request,
            'users/supervisors_create.html',
            {'form': form})


@login_required
def seller_detail_view(request, pk):
    seller = Seller.objects.get(id=pk)
    if request.method == 'POST':
        form = SellerEditForm(request.POST)
        if form.is_valid():
            seller.first_name = form.cleaned_data["first_name"]
            seller.last_name = form.cleaned_data["last_name"]
            seller.email = form.cleaned_data["email"]
            seller.phone = form.cleaned_data["phone"]
            seller.is_active = form.cleaned_data["is_active"]
            seller.save()
            return redirect('/')
        else:
            return render(request, 'users/seller_detail.html', {'form': form, 'pk': pk})
    else:
        form = SellerEditForm(instance=seller)
        return render(request, 'users/seller_detail.html', {'form': form, 'pk': pk})


@login_required
def supervisor_detail_view(request, pk):
    supervisor = Supervisor.objects.get(id=pk)
    if request.method == 'POST':
        form = SellerEditForm(request.POST)
        if form.is_valid():
            supervisor.first_name = form.cleaned_data["first_name"]
            supervisor.last_name = form.cleaned_data["last_name"]
            supervisor.email = form.cleaned_data["email"]
            supervisor.phone = form.cleaned_data["phone"]
            supervisor.is_active = form.cleaned_data["is_active"]
            supervisor.save()
            return redirect('supervisors_overview')
        else:
            return render(request, 'users/supervisor_detail.html', {'form': form, 'pk': pk})
    else:
        form = SupervisorCreateForm(instance=supervisor)
        return render(request, 'users/supervisor_detail.html', {'form': form, 'pk': pk})


@login_required
def user_delete(request, pk):
    user = User.objects.get(id=pk)
    user.is_active = False
    user.save()
    return redirect('sellers_overview')


@login_required
def user_hard_delete(request, pk):
    user = User.objects.get(id=pk)
    if user.is_supervisor():
        user.delete()
        return redirect('supervisors_overview')
    elif user.is_seller():
        user.delete()
        return redirect('sellers_overview')


@login_required
def user_profile(request, pk):
    user = User.objects.get(id=pk)
    if request.method == 'POST':
        form = UserProfileForm(request.POST)
        if form.is_valid():
            user.first_name = form.cleaned_data["first_name"]
            user.last_name = form.cleaned_data["last_name"]
            user.email = form.cleaned_data["email"]
            user.phone = form.cleaned_data["phone"]
            user.save()
            return redirect('/')
        else:
            return render(request, 'users/profile.html', {'form': form, 'pk': pk})
    else:
        form = UserProfileForm(instance=user)
        form2 = PasswordResetForm()
        return render(request, 'users/profile.html', {'form': form, 'pk': pk, 'form2': form2})


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully changed!')
            return redirect('change_password')
        else:
            messages.error(request, 'Please correct the error(s) found.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {'form': form})


class SellerAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):

        if not self.request.user.is_authenticated():
            return Seller.objects.none()

        sellers = self.forwarded.get('sellers', None)
        qs = Seller.objects.filter(id__in=sellers.values_list('id').flatten(), is_deleted=False)

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs
