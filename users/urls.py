from django.conf.urls import url
from users import views as users_views


urlpatterns = [
    url(r'^$', users_views.index, name='index'),
    url(r'^user_profile/(?P<pk>\d+)$', users_views.user_profile, name='user_profile'),
    url(r'^seller_customers/(?P<pk>\d+)$', users_views.customer_delete, name='customer_delete'),
    url(r'^sellers/(?P<pk>\d+)$', users_views.seller_detail_view, name='seller-detail'),
    url(r'^supervisors/(?P<pk>\d+)$', users_views.supervisor_detail_view, name='supervisor-detail'),
    url(r'^customers/(?P<pk>\d+)$', users_views.customer_detail_view, name='customer-detail'),
    url(r'^users/delete/(?P<pk>\d+)$', users_views.user_delete, name='user-delete'),
    url(r'^users/hard_delete/(?P<pk>\d+)$', users_views.user_hard_delete, name='user_hard_delete'),
    url(r'^sellers_create', users_views.sellers_create, name='sellers_create'),
    url(r'^supervisors_create', users_views.supervisors_create, name='supervisors_create'),
    url(r'^create_customer', users_views.create_customer, name='create_customer'),
    url(r'^seller_customers', users_views.sellers_customers, name='seller_customers'),
    url(r'^sellers', users_views.sellers_overview, name='sellers_overview'),
    url(r'^supervisors', users_views.supervisors_overview, name='supervisors_overview'),
    url(r'^password/$', users_views.change_password, name='change_password'),
]
