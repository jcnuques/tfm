$(function () {
    $('.datetime-input').datetimepicker({
        format:'YYYY-MM-DD'
    });
});

$(document).on('click', '.confirm-save', function(){
    return confirm('Are you sure you want to save this?');
})

$(document).on('click', '.confirm-delete', function(){
    return confirm('Are you sure you want to delete this?');
})