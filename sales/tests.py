# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpRequest
from pyasn1.type.tag import Tag

from sales.models import *
from users.models import *
from companies.models import *
from sales.views import sales_register
from django.test import TestCase, Client


# Create your tests here.

class CreateSaleTest(TestCase):
    def setUp(self):
        self.client = Client()

        company = Company.objects.create(name='Company 1', address='direccion', phone='+34512302547',
                                         email='company1@email.com', created_at='2019-01-01', is_deleted=False)
        company.save()

        seller = Seller.objects.create(first_name='Juanito', last_name='Alimaña', email='juanito@gmail.com',
                                       username='juanito@gmail.com', phone='3412547888', is_active=True,
                                       company=company)
        seller.set_password('test2019')
        seller.save()

        seller2 = Seller.objects.create(first_name='asdad', last_name='asdasd', email='asd@gmail.com',
                                        username='asd@gmail.com', phone='3412547888', is_active=True,
                                        company=company)
        seller2.set_password('test2019')
        seller2.save()

        seller3 = Seller.objects.create(first_name='qqqq', last_name='qqq', email='qqq@gmail.com',
                                        username='qqq@gmail.com', phone='3412547888', is_active=True,
                                        company=company)
        seller3.set_password('test2019')
        seller3.save()

        supervisor = Supervisor.objects.create(first_name='asda', last_name='asdad', email='sup@gmail.com',
                                               username='sup@gmail.com', phone='3412547888', is_active=True,
                                               company=company)
        supervisor.set_password('test2019')
        supervisor.save()

        Customer.objects.create(name='Customer 1', identification='Y4965248E', address='Clariano 12',
                                phone='+34617878669', email='jcnuques@gmail.com', created_at='2019-01-01',
                                is_deleted=False, company=company, seller=seller).save()

        SaleObjective.objects.create(seller=seller, supervisor=supervisor, sale_objective='20000',
                                     date_from='2019-08-01', date_to='2019-08-31').save()

        SaleObjective.objects.create(seller=seller2, supervisor=supervisor, sale_objective='50000',
                                     date_from='2019-08-01', date_to='2019-08-31').save()

        SaleObjective.objects.create(seller=seller3, supervisor=supervisor, sale_objective='90000',
                                     date_from='2019-08-01', date_to='2019-08-31').save()

    def test_create_sale(self):
        customer = Customer.objects.filter(name='Customer 1').get()

        sale_attributes = {
            'reference': 'TEST',
            'customer': str(customer.id),
            'amount': '9999',
            'sale_date': '2019-08-30'
        }
        self.client.login(username='juanito@gmail.com', password='test2019')
        response = self.client.post('/sales_register/', sale_attributes)

        sale = Sale.objects.filter(reference='TEST').get()
        self.assertEqual(response.status_code, 302)

        values = []
        for (field, val) in sale:
            if field in sale_attributes.keys():
                if field == 'customer':
                    values.append(str(val.id))
                elif field == 'amount':
                    values.append(str(int(val)))
                else:
                    values.append(str(val))

        self.assertEqual(sorted(sale_attributes.values()), sorted(values))

    def test_negative_create_sale(self):
        customer = Customer.objects.filter(name='Customer 1').get()

        sale_attributes = {
            'reference': 'TEST',
            'customer': str(customer.id),
            'amount': '-9999',
            'sale_date': '2019-08-30'
        }
        self.client.login(username='juanito@gmail.com', password='test2019')
        response = self.client.post('/sales_register/', sale_attributes)

        sale = Sale.objects.filter(reference='TEST').exists()
        self.assertEqual(response.status_code, 200)

        self.assertEqual(False, sale)

    def test_create_sales_objective(self):
        seller = Seller.objects.filter(username='juanito@gmail.com').get()

        attributes = {
            'seller': str(seller.id),
            'sale_objective': '9999',
            'date_from': '2019-09-01',
            'date_to': '2019-09-30'
        }

        self.client.login(username='sup@gmail.com', password='test2019')
        response = self.client.post('/sales_objective_create/', attributes)

        sale_objective = SaleObjective.objects.filter(date_from='2019-09-01', date_to='2019-09-30').get()
        self.assertEqual(response.status_code, 302)

        values = []
        for (field, val) in sale_objective:
            if field in attributes.keys():
                if field == 'seller':
                    values.append(str(val.id))
                elif field == 'supervisor':
                    values.append(str(val.id))
                elif field == 'sale_objective':
                    values.append(str(int(val)))
                else:
                    values.append(str(val))

        self.assertEqual(sorted(attributes.values()), sorted(values))

    def test_negative_create_sales_objective(self):
        seller = Seller.objects.filter(username='juanito@gmail.com').get()

        attributes = {
            'seller': str(seller.id),
            'sale_objective': '-9999',
            'date_from': '2019-09-01',
            'date_to': '2019-09-30'
        }

        self.client.login(username='sup@gmail.com', password='test2019')
        response = self.client.post('/sales_objective_create/', attributes)

        sale_objective = SaleObjective.objects.filter(date_from='2019-09-01', date_to='2019-09-30').exists()
        self.assertEqual(response.status_code, 200)

        self.assertEqual(False, sale_objective)

    # def test_get_company_sales_list(self):
    #     attributes = {
    #         'date_from': '2019-08-01',
    #         'date_to': '2019-08-31'
    #     }
    #
    #     self.client.login(username='sup@gmail.com', password='test2019')
    #     response = self.client.post('/sales_objective_history/', attributes)
    #     request = response.wsgi_request
    #
    #     sales_objective_list = SaleObjective.objects.filter(date_from='2019-08-01', date_to='2019-08-31')
    #     # self.assertEqual(response.status_code, 200)
    #     #
    #     # self.assertEqual(sales_objective_list, request)
