# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from sales.models import Sale, SaleObjective
from users.models import Seller


class SaleAdmin(admin.ModelAdmin):
    model = Sale
    exclude = ("created_at",)
    list_display = ('customer', 'seller', 'sale_date', 'amount', 'created_at', 'is_deleted')

    def get_queryset(self, request):
        qs = super(SaleAdmin, self).get_queryset(request)
        sellers = Seller.objects.filter(company=request.user.company_id)
        if request.user.is_superuser:
            return qs
        return qs.filter(seller__in=sellers)


class SaleObjectiveAdmin(admin.ModelAdmin):
    model = SaleObjective
    exclude = ('created_at',)
    list_display = ('seller', 'supervisor', 'sale_objective', 'date_from', 'date_to', 'is_deleted')

    def get_queryset(self, request):
        qs = super(SaleObjectiveAdmin, self).get_queryset(request)
        sellers = Seller.objects.filter(company=request.user.company_id)
        if request.user.is_superuser:
            return qs
        return qs.filter(seller__in=sellers)


# Register your models here.
admin.site.register(Sale, SaleAdmin)
admin.site.register(SaleObjective, SaleObjectiveAdmin)
