import self as self
from django import forms
from .models import *
from dal import autocomplete
import datetime


class SalesRegisterForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SalesRegisterForm, self).__init__(*args, **kwargs)
        # self.fields['customer'].queryset = Customer.objects.filter(seller=kwargs['initial']['seller'],
        #                                                            is_deleted=False)
        self.fields['customer'].queryset = Customer.objects.filter(company_id=kwargs['initial']['company'],)
        self.fields['seller'].queryset = Seller.objects.filter(company_id=kwargs['initial']['company'],)

    class Meta:
        model = Sale
        fields = ['reference', 'customer', 'seller', 'amount', 'sale_date']
        widgets = {
            'sale_date': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'customer': autocomplete.ModelSelect2(),
            'seller': autocomplete.ModelSelect2(),
        }


class SalesHistorySearchForm(forms.Form):
    date_from = forms.DateField(widget=forms.DateTimeInput(
        attrs={'class': 'datetime-input'}),
        initial=datetime.datetime.now())
    date_to = forms.DateField(widget=forms.DateTimeInput(
        attrs={'class': 'datetime-input'}),
        initial=datetime.datetime.now())
    search_field = forms.CharField(max_length=100, required=False, label="Search by Reference/Customer/Seller")


class SalesObjectivesHistoryForm(forms.Form):
    date_from = forms.DateField(widget=forms.DateTimeInput(
        attrs={'class': 'datetime-input'}),
        initial=datetime.datetime.now())
    date_to = forms.DateField(widget=forms.DateTimeInput(
        attrs={'class': 'datetime-input'}),
        initial=datetime.datetime.now())
    search_field = forms.CharField(max_length=100, required=False, label="Search by Seller/Creator")


class SalesObjectiveCreateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(SalesObjectiveCreateForm, self).__init__(*args, **kwargs)
        self.fields['seller'].queryset = Seller.objects.filter(company_id=kwargs['initial']['company_id'],
                                                               is_active=True)

    class Meta:
        model = SaleObjective
        fields = ['seller', 'sale_objective', 'date_from', 'date_to']

        widgets = {
            'date_from': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'date_to': forms.DateTimeInput(attrs={'class': 'datetime-input'}),
            'seller': autocomplete.ModelSelect2(),
        }
