from django.conf.urls import url
from sales import views as sales_views


urlpatterns = [
    url(r'^sales_objective_history', sales_views.sales_objective_history, name='sales_objective_history'),
    url(r'^sales_objective_create', sales_views.sales_objective_create, name='sales_objective_create'),
    url(r'^sales_register', sales_views.sales_register, name='sales_register'),
    url(r'^sales_delete/(?P<pk>\d+)$', sales_views.sale_delete, name='sale_delete'),
    url(r'^sales_objective_delete/(?P<pk>\d+)$', sales_views.sales_objective_delete, name='sales-objective-delete'),
    url(r'^sales/(?P<pk>\d+)$', sales_views.sale_detail_view, name='sale-detail'),
    url(r'^sales_objectives/(?P<pk>\d+)$', sales_views.sale_objective_detail_view, name='sales-objective-detail'),
    url(r'^sales', sales_views.sales, name='sales'),
    url(r'^customer-autocomplete/$', sales_views.CustomerAutocomplete.as_view(), name='customer-autocomplete'),
    url(r'^seller-autocomplete/$', sales_views.SellerAutocomplete.as_view(), name='seller-autocomplete'),
    url(r'^graph', sales_views.line_chart, name='graph'),
    url(r'^line_chart_json', sales_views.line_chart_json, name='line_chart_json'),
]
