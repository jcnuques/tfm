# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.db import models
from users.models import Seller, Customer, Supervisor
from django.db.models import Sum, Q
from django.urls import reverse
from django.core.validators import MinValueValidator
import datetime


class Sale(models.Model):
    sale_date = models.DateField(auto_now_add=False)
    created_at = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=12, decimal_places=2, null=False, blank=False,
                                 validators=[MinValueValidator(1)])
    customer = models.ForeignKey(Customer)
    seller = models.ForeignKey(Seller)
    reference = models.CharField(max_length=11, blank=True, null=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'sale'
        verbose_name_plural = 'sales'

    def get_absolute_url(self):
        """
        Retorna la url para acceder a una instancia particular de una venta.
        """
        return reverse('sale-detail', args=[str(self.id)])

    def __iter__(self):
        field_names = [f.name for f in self._meta.fields]
        for field_name in field_names:
            value = getattr(self, field_name, None)
            yield (field_name, value)


class SaleObjective(models.Model):
    seller = models.ForeignKey(Seller)
    supervisor = models.ForeignKey(Supervisor)
    sale_objective = models.DecimalField(max_digits=11, decimal_places=2, null=False, blank=False,
                                         validators=[MinValueValidator(1)])
    date_from = models.DateField()
    date_to = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)

    def get_absolute_url(self):
        """
        Retorna la url para acceder a una instancia particular de una venta.
        """
        return reverse('sales-objective-detail', args=[str(self.id)])

    class Meta:
        verbose_name = 'sale objective'
        verbose_name_plural = 'sales objectives'

    def __iter__(self):
        field_names = [f.name for f in self._meta.fields]
        for field_name in field_names:
            value = getattr(self, field_name, None)
            yield (field_name, value)


def get_seller_sales_count(current_user_id, date_from, date_to, search):
    if date_from is None and date_to is None and search is None:
        sales_count = Sale.objects.filter(seller__exact=current_user_id,
                                          is_deleted__exact=False,
                                          sale_date__year=datetime.date.today().year,
                                          sale_date__month=datetime.date.today().month, ).count()
    elif search is None:
        sales_count = Sale.objects.filter(seller__exact=current_user_id,
                                          is_deleted__exact=False,
                                          sale_date__range=[date_from, date_to]).count()
    else:
        sales_count = Sale.objects.filter(seller__exact=current_user_id,
                                          is_deleted__exact=False,
                                          sale_date__range=[date_from, date_to],
                                          customer__name__icontains=search).count()
    return sales_count


def get_seller_sales_amount(current_user_id, date_from, date_to, search):
    if date_from is None and date_to is None and search is None:
        sales_amount = Sale.objects.filter(seller__exact=current_user_id,
                                           is_deleted__exact=False,
                                           sale_date__year=datetime.date.today().year,
                                           sale_date__month=datetime.date.today().month,
                                           ).aggregate(Sum("amount"))['amount__sum']
    elif search is None:
        sales_amount = Sale.objects.filter(seller__exact=current_user_id,
                                           is_deleted__exact=False,
                                           sale_date__range=[date_from, date_to]).aggregate(Sum("amount"))[
            'amount__sum']
    else:
        sales_amount = Sale.objects.filter(seller__exact=current_user_id,
                                           is_deleted__exact=False,
                                           sale_date__range=[date_from, date_to],
                                           customer__name__icontains=search).aggregate(Sum("amount"))[
            'amount__sum']
    return sales_amount


def get_seller_sales_list(current_user_id, date_from, date_to, search):
    if date_from is None and date_to is None and search is None:
        sales_list = Sale.objects.filter(seller__exact=current_user_id,
                                         sale_date__year=datetime.date.today().year,
                                         sale_date__month=datetime.date.today().month,
                                         is_deleted__exact=False).order_by('-sale_date')
    elif search is None:
        sales_list = Sale.objects.filter(seller__exact=current_user_id,
                                         is_deleted__exact=False,
                                         sale_date__range=[date_from, date_to]).order_by('-sale_date')
    else:
        sales_list = Sale.objects.filter(Q(seller__exact=current_user_id) &
                                         Q(is_deleted__exact=False) &
                                         Q(sale_date__range=[date_from, date_to]) &
                                         (Q(customer__name__icontains=search) |
                                          Q(reference__icontains=search))).order_by('-sale_date')
    return sales_list


def get_company_sales_list(company, date_from, date_to, search):
    if search is None:
        sales_list = Sale.objects.filter(seller__company=company,
                                         is_deleted__exact=False,
                                         sale_date__range=[date_from, date_to]).order_by('-sale_date')
    else:
        sales_list = Sale.objects.filter(Q(seller__company=company) &
                                         Q(is_deleted__exact=False) &
                                         Q(sale_date__range=[date_from, date_to]) &
                                         (Q(customer__name__icontains=search) |
                                          Q(reference__icontains=search) |
                                          Q(seller__username__icontains=search))).order_by('-sale_date')
    return sales_list


def get_seller_sales_objective(current_user_id, date_from, date_to):
    if date_from is None and date_to is None:
        result = SaleObjective.objects.filter(seller__exact=current_user_id,
                                              date_from__year=datetime.date.today().year,
                                              date_from__month=datetime.date.today().month,
                                              date_to__year=datetime.date.today().year,
                                              date_to__month=datetime.date.today().month,
                                              is_deleted__exact=False).values('sale_objective')
        if not result:
            sale_objective = None
        else:
            sale_objective = result[0].get('sale_objective')
    else:
        sale_objective = SaleObjective.objects.filter(seller__exact=current_user_id,
                                                      date_from__lte=date_to,
                                                      date_to__gte=date_from,
                                                      is_deleted=False)

    return sale_objective


def get_company_sellers_sales_objectives(company, date_from, date_to, search):
    if search is None:
        sales_objectives = SaleObjective.objects.filter(seller__company=company,
                                                        date_from__lte=date_to,
                                                        date_to__gte=date_from,
                                                        is_deleted=False).order_by('-date_from')
    else:
        sales_objectives = SaleObjective.objects.filter(Q(seller__company=company) &
                                                        Q(date_from__lte=date_to) &
                                                        Q(date_to__gte=date_from) &
                                                        Q(is_deleted=False) &
                                                        (Q(seller__username__icontains=search) |
                                                         Q(supervisor__username__icontains=search))
                                                        ).order_by('-date_from')
    return sales_objectives
