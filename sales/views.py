from __future__ import unicode_literals

from django.shortcuts import render, redirect
from companies.models import Company
from .forms import *
from users.models import *
from companies.models import *
from sales.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from dal import autocomplete


@login_required
def sales(request):
    # is_seller = Seller.objects.filter(id=request.user.id)
    # is_supervisor = Supervisor.objects.filter(id=request.user.id)

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SalesHistorySearchForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            if request.user.is_seller():
                # if is_seller:
                date_from = form.cleaned_data['date_from']
                date_to = form.cleaned_data['date_to']
                search = form.cleaned_data['search_field']
                sales_list = get_seller_sales_list(request.user.id, date_from, date_to, search)
                sales_count = get_seller_sales_count(request.user.id, date_from, date_to, search)
                sales_amount = get_seller_sales_amount(request.user.id, date_from, date_to, search)
                return render(
                    request,
                    'sales/sales.html',
                    context={'sales_count': sales_count,
                             'sales_amount': sales_amount,
                             'sales_list': sales_list,
                             'form': form},
                )
            elif request.user.is_supervisor():
                # elif is_supervisor:
                date_from = form.cleaned_data['date_from']
                date_to = form.cleaned_data['date_to']
                search = form.cleaned_data['search_field']
                sales_list = get_company_sales_list(request.user.company, date_from, date_to, search)
                return render(
                    request,
                    'sales/sales.html',
                    context={
                        'sales_list': sales_list,
                        'form': form
                    },
                )
    # if a POST (or any other method) we'll create a blank form
    else:
        # if is_seller:
        #     print "True. User is Seller"
        # else:
        #     print "False. Is not a seller"
        #
        # if is_supervisor:
        #     print "True. User is supervisor"
        # else:
        #     print "False. User is not a supervisor"
        form = SalesHistorySearchForm()
        return render(
            request,
            'sales/sales.html',
            {'form': form},
        )


@login_required
def sales_register(request):
    initial_data = {'seller': Seller.objects.filter(id__exact=request.user.id).get(),
                    'company': request.user.company.id,
                    'sale_date': datetime.date.today(),}
    # initial_data = {'company': request.user.company.id, }
    # company = Company.objects.filter(name__exact=request.user.company).get()
    # seller = Seller.objects.filter(id=request.user.id).get()
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SalesRegisterForm(request.POST, initial=initial_data)
        # check whether it's valid:
        if form.is_valid():
            sale = Sale()
            sale.reference = form.cleaned_data['reference']
            sale.sale_date = form.cleaned_data['sale_date']
            sale.amount = form.cleaned_data['amount']
            sale.is_deleted = False
            sale.customer = form.cleaned_data['customer']
            # sale.seller = Seller.objects.filter(id__exact=request.user.id).get()
            sale.seller = form.cleaned_data['seller']
            sale.save()
            return redirect('/')
        else:
            return render(
                request,
                'sales/sales_register.html',
                {'form': form})
    else:
        form = SalesRegisterForm(initial=initial_data)
        return render(
            request,
            'sales/sales_register.html',
            {'form': form})


class CustomerAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):

        if not self.request.user.is_authenticated():
            return Customer.objects.none()

        customers = self.forwarded.get('customer', None)
        qs = Customer.objects.filter(id__in=customers.values_list('id').flatten(), is_deleted=False)

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs


class SellerAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):

        if not self.request.user.is_authenticated():
            return Seller.objects.none()

        company_id = self.forwarded.get('company_id', None)
        print company_id

        qs = Seller.objects.filter(company=company_id, is_active=True)

        if self.q:
            qs = qs.filter(username__icontains=self.q)

        return qs


@login_required()
def sales_objective_history(request):
    if request.method == 'POST':
        form = SalesObjectivesHistoryForm(request.POST)
        if form.is_valid():
            if request.user.is_seller():
                date_from = form.cleaned_data['date_from']
                date_to = form.cleaned_data['date_to']
                sales_objective_list = get_seller_sales_objective(request.user.id, date_from, date_to)
                for sale_objective in sales_objective_list:
                    sale_objective.sales_amount = get_seller_sales_amount(request.user.id,
                                                                          sale_objective.date_from,
                                                                          sale_objective.date_to,
                                                                          None)
                return render(
                    request,
                    'sales/sales_objective_history.html',
                    context={'sales_objective_list': sales_objective_list,
                             'form': form},
                )
            elif request.user.is_supervisor():
                date_from = form.cleaned_data['date_from']
                date_to = form.cleaned_data['date_to']
                search = form.cleaned_data['search_field']
                sales_objective_list = get_company_sellers_sales_objectives(request.user.company, date_from, date_to,
                                                                            search)
                for sale_objective in sales_objective_list:
                    sale_objective.sales_amount = get_seller_sales_amount(sale_objective.seller.id,
                                                                          sale_objective.date_from,
                                                                          sale_objective.date_to,
                                                                          None)
                return render(
                    request,
                    'sales/sales_objective_history.html',
                    context={'sales_objective_list': sales_objective_list,
                             'form': form},
                )
    else:
        initial_data = {'date_from': datetime.datetime.now(),
                        'date_to': datetime.datetime.now()}
        form = SalesObjectivesHistoryForm(initial=initial_data)
        return render(
            request,
            'sales/sales_objective_history.html',
            {'form': form},
        )


@login_required()
def sale_detail_view(request, pk):
    # if request.user.is_seller():
        # initial_data = {'seller': Seller.objects.filter(id__exact=request.user.id).get(), }
    initial_data = {'company': request.user.company.id, }
    # else:
    #     initial_data = {}
    sale = Sale.objects.get(id=pk)
    if request.method == 'POST':
        form = SalesRegisterForm(request.POST, initial=initial_data)
        if form.is_valid():
            sale.seller = form.cleaned_data["seller"]
            sale.customer = form.cleaned_data["customer"]
            sale.amount = form.cleaned_data["amount"]
            sale.sale_date = form.cleaned_data["sale_date"]
            sale.reference = form.cleaned_data["reference"]
            sale.save()
            return redirect('/')
        else:
            return render(request, 'sales/sale_detail.html', {'form': form, 'pk': pk})
    else:
        form = SalesRegisterForm(instance=sale, initial=initial_data)
        return render(request, 'sales/sale_detail.html', {'form': form, 'pk': pk})


@login_required()
def sale_delete(request, pk):
    sale = Sale.objects.get(id=pk)
    sale.is_deleted = True
    sale.save()
    return redirect('/')


@login_required()
def sales_objective_create(request):
    initial_data = {'supervisor': Supervisor.objects.filter(id__exact=request.user.id).get(),
                    'company_id': Company.objects.filter(id__exact=request.user.company.id).get().id}
    if request.method == 'POST':
        form = SalesObjectiveCreateForm(request.POST, initial=initial_data)
        if form.is_valid():
            seller = form.cleaned_data['seller']
            date_from = form.cleaned_data['date_from']
            date_to = form.cleaned_data['date_to']
            sale_objective = get_seller_sales_objective(seller, date_from, date_to)
            if sale_objective:
                error_message = 'A Sales Objective has already been assigned to the seller for the chosen period'
                return render(request,
                              'sales/sales_objective_create.html',
                              {'form': form,
                               'error_message': error_message}
                              )
            elif date_from > date_to:
                error_message = "Date to can not be greater than Date From"
                return render(request,
                              'sales/sales_objective_create.html',
                              {'form': form,
                               'error_message': error_message}
                              )
            else:
                sale_objective = SaleObjective()
                sale_objective.seller = seller
                sale_objective.sale_objective = form.cleaned_data['sale_objective']
                sale_objective.date_from = date_from
                sale_objective.date_to = date_to
                sale_objective.supervisor = Supervisor.objects.filter(id__exact=request.user.id).get()
                sale_objective.save()

                return redirect("/")
        else:
            return render(
                request,
                'sales/sales_objective_create.html',
                {'form': form})
    else:
        form = SalesObjectiveCreateForm(initial=initial_data)
        return render(request, 'sales/sales_objective_create.html', {'form': form})


@login_required()
def sale_objective_detail_view(request, pk):
    initial_data = {'supervisor': Supervisor.objects.filter(id__exact=request.user.id).get(),
                    'company_id': request.user.company.id}
    sale_objective = SaleObjective.objects.get(id=pk)
    if request.method == 'POST':
        form = SalesObjectiveCreateForm(request.POST, initial=initial_data)
        if form.is_valid():
            sale_objective.seller = form.cleaned_data['seller']
            sale_objective.supervisor = Supervisor.objects.filter(id__exact=request.user.id).get()
            sale_objective.sale_objective = form.cleaned_data['sale_objective']
            sale_objective.date_from = form.cleaned_data['date_from']
            sale_objective.date_to = form.cleaned_data['date_to']
            sale_objective.save()
            return redirect('/')
        else:
            return render(request, 'sales/sales_objective_detail.html', {'form': form, 'pk': pk})
    else:
        form = SalesObjectiveCreateForm(initial=initial_data, instance=sale_objective)
        return render(request, 'sales/sales_objective_detail.html', {'form': form, 'pk': pk})


def sales_objective_delete(request, pk):
    sales_objective = SaleObjective.objects.get(id=pk)
    sales_objective.is_deleted = True
    sales_objective.save()
    return redirect('sales_objective_history')


from random import randint
from django.views.generic import TemplateView
from chartjs.views.lines import BaseLineChartView


class LineChartJSONView(BaseLineChartView):
    def get_labels(self):
        """Return 7 labels for the x-axis."""
        return ["January", "February", "March", "April", "May", "June", "July"]

    def get_providers(self):
        """Return names of datasets."""
        return ["Central", "Eastside", "Westside"]

    def get_data(self):
        """Return 3 datasets to plot."""

        return [[75, 44, 92, 11, 44, 95, 35],
                [41, 92, 18, 3, 73, 87, 92],
                [87, 21, 94, 3, 90, 13, 65]]


line_chart = TemplateView.as_view(template_name='graphs/graph.html')
line_chart_json = LineChartJSONView.as_view()
