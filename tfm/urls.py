"""tfm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    url(r'^admin/', admin.site.urls),
    url(r'', include('companies.urls')),
    url(r'', include('users.urls')),
    url(r'', include('sales.urls')),
    url(r'^auth/', include('django.contrib.auth.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# urlpatterns = [
#     url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
#     url(r'^admin/', admin.site.urls),
#     # path('tests/', include('tests.urls')),
#     url(r'', include('sales.urls')),
#     url(r'', include('users.urls')),
#     # url(r'^sales_objective_history', sales_views.sales_objective_history, name='sales_objective_history'),
#     # url(r'^sales_objective_create', sales_views.sales_objective_create, name='sales_objective_create'),
#     # url(r'^sales_register', sales_views.sales_register, name='sales_register'),
#     # url(r'^delete/(?P<pk>\d+)$', sales_views.sale_delete, name='sale_delete'),
#     # url(r'^sales/(?P<pk>\d+)$', sales_views.sale_detail_view, name='sale-detail'),
#     # # url(r'^sales', sales_views.sales, name='sales'),
#     # url(r'^seller_customers/(?P<pk>\d+)$', users_views.customer_delete, name='customer_delete'),
#     # url(r'^sellers/(?P<pk>\d+)$', users_views.seller_detail_view, name='seller-detail'),
#     # url(r'^customers/(?P<pk>\d+)$', users_views.customer_detail_view, name='customer-detail'),
#     # url(r'^sellers_create', users_views.sellers_create, name='sellers_create'),
#     # url(r'^create_customer', users_views.create_customer, name='create_customer'),
#     # url(r'^seller_customers', users_views.sellers_customers, name='seller_customers'),
#     url(r'^auth/', include('django.contrib.auth.urls')),
#     # url(r'^customer-autocomplete/$', sales_views.CustomerAutocomplete.as_view(), name='customer-autocomplete'),
#     # url(r'^company-autocomplete/$', users_views.CompanyAutocomplete.as_view(), name='company-autocomplete'),
# ]
